import { useEffect, useState } from "react";
import { Footer, Header, BookingItem } from "../../components";

export const Bookings = () => {
  interface IBooking {
    id: string;
    title: string;
    guests: number;
    totalPrice: number;
    image: string;
    date: string;
  }

  const [bookings, setBookings] = useState<IBooking[]>([]);

  const handleCancelBooking = (id: string) => {
    setBookings((prevBookings) => {
      const updatedBookings = prevBookings.filter(
        (booking) => booking.id !== id
      );

      localStorage.setItem("bookings", JSON.stringify(updatedBookings));

      return updatedBookings;
    });
  };

  useEffect(() => {
    const existingBookingsJSON = localStorage.getItem("bookings");
    const existingBookings = existingBookingsJSON
      ? JSON.parse(existingBookingsJSON)
      : [];

    existingBookings.sort(
      (a: IBooking, b: IBooking) =>
        new Date(a.date).getTime() - new Date(b.date).getTime()
    );

    setBookings(existingBookings);
  }, []);

  return (
    <>
      <Header />
      <main className="bookings-page">
        <h1 className="visually-hidden">Travel App</h1>
        <ul className="bookings__list">
          {bookings.map((booking) => (
            <BookingItem
              key={booking.id}
              id={booking.id}
              title={booking.title}
              guests={booking.guests}
              date={booking.date}
              totalPrice={booking.totalPrice}
              onCancel={handleCancelBooking}
            />
          ))}
        </ul>
      </main>
      <Footer />
    </>
  );
};
