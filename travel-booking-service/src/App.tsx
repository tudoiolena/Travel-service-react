import { BrowserRouter, Route, Routes } from "react-router-dom";
import { Paths } from "./enums/paths";
import { Bookings, Main, SignIn, SignUp, Trip } from "./pages";

const App = () => (
  <BrowserRouter>
    <Routes>
      <Route path={Paths.MAIN} element={<Main />} />
      <Route path={Paths.LOGIN} element={<SignIn />} />
      <Route path={Paths.REGISTER} element={<SignUp />} />
      <Route path={`${Paths.TRIP}/:id`} element={<Trip />} />
      <Route path={Paths.BOOKINGS} element={<Bookings />} />
    </Routes>
  </BrowserRouter>
);

export default App;
