import { Paths } from "../../enums/paths";
import { Link, useNavigate } from "react-router-dom";
import { useForm, SubmitHandler } from "react-hook-form";
import { AuthButton, AuthInput, Footer, Header } from "../../components";

interface IFormValues {
  name?: string;
  email: string;
  password: string;
}

export const SignUp = () => {
  const navigate = useNavigate();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<IFormValues>({
    defaultValues: {
      name: "",
      email: "",
      password: "",
    },
  });

  const onSubmit: SubmitHandler<IFormValues> = () => {
    navigate(Paths.MAIN);
  };

  return (
    <>
      <Header />
      <main className="sign-up-page">
        <h1 className="visually-hidden">Travel App</h1>
        <form
          onSubmit={handleSubmit(onSubmit)}
          className="sign-up-form"
          autoComplete="off"
        >
          <h2 className="sign-up-form__title">Sign Up</h2>
          <AuthInput
            label="Full name"
            testId="auth-full-name"
            name="name"
            register={register}
            errors={errors}
            validationSchema={{
              required: "Full name is required",
            }}
          />
          <AuthInput
            label="Email"
            testId="auth-email"
            name="email"
            type="email"
            register={register}
            errors={errors}
            validationSchema={{
              required: "Email is required",
            }}
          />
          <AuthInput
            label="Password"
            testId="auth-password"
            name="password"
            type="password"
            autoComplete="new-password"
            register={register}
            errors={errors}
            validationSchema={{
              required: "Password is required",
              minLength: {
                value: 3,
                message: "Password must be at least 3 charachters long",
              },
              maxLength: {
                value: 20,
                message: "Password must be at most 20 characters",
              },
            }}
          />
          <AuthButton label="Sign In" />
          <span>
            Already have an account?
            <Link
              data-test-id="auth-sign-in-link"
              to={Paths.LOGIN}
              className="sign-up-form__link"
            >
              Sign In
            </Link>
          </span>
        </form>
      </main>
      <Footer />
    </>
  );
};
