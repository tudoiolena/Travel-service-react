import { ChangeEvent, useEffect, useState } from "react";
import {
  Footer,
  Header,
  SearchInput,
  SearchSelect,
  TripCard,
} from "../../components";
import tripsData from "../../trips.json";

export const Main = () => {
  interface ITrip {
    title: string;
    duration: number;
    level: string;
    price: number;
    id: string;
    image: string;
  }

  const [search, setSearch] = useState("");
  const [allTrips, setAllTrips] = useState<ITrip[]>([]);
  const [selectedDurationRange, setSelectedDurationRange] = useState<
    string | null
  >(null);
  const [selectedLevel, setSelectedLevel] = useState<string | null>(null);

  const filteredTrips = allTrips.filter((trip) => {
    const titleMatches = trip.title
      .toLowerCase()
      .includes(search.toLowerCase());

    const durationMatches =
      !selectedDurationRange ||
      (selectedDurationRange === "0_x_5" && trip.duration < 5) ||
      (selectedDurationRange === "5_x_10" &&
        trip.duration >= 5 &&
        trip.duration < 10) ||
      (selectedDurationRange === "10" && trip.duration >= 10);

    const levelMatches =
      !selectedLevel ||
      (selectedLevel === "easy" && trip.level === "easy") ||
      (selectedLevel === "moderate" && trip.level === "moderate") ||
      (selectedLevel === "difficult" && trip.level === "difficult");

    return titleMatches && durationMatches && levelMatches;
  });

  const durationOptions = [
    { value: "0_x_5", label: "< 5 days" },
    { value: "5_x_10", label: "< 10 days" },
    { value: "10", label: "≥ 10 days" },
  ];

  const levelOptions = [
    { value: "", label: "level" },
    { value: "easy", label: "easy" },
    { value: "moderate", label: "moderate" },
    { value: "difficult", label: "difficult" },
  ];

  const handleSearchChange = (event: ChangeEvent<HTMLInputElement>) => {
    setSearch(event.target.value);
  };

  const handleDurationChange = (event: ChangeEvent<HTMLSelectElement>) => {
    const selectedValue = event.target.value;
    setSelectedDurationRange(selectedValue);
  };

  const handleLevelChange = (event: ChangeEvent<HTMLSelectElement>) => {
    const selectedValue = event.target.value;
    setSelectedLevel(selectedValue);
  };

  useEffect(() => {
    setAllTrips(tripsData);
  }, []);

  return (
    <>
      <Header />

      <main>
        <h1 className="visually-hidden">Travel App</h1>
        <section className="trips-filter">
          <h2 className="visually-hidden">Trips filter</h2>
          <form className="trips-filter__form" autoComplete="off">
            <SearchInput onChange={handleSearchChange} />
            <SearchSelect
              label="Search by duration"
              options={durationOptions}
              dataTestId="filter-duration"
              name="duration"
              onChange={handleDurationChange}
            />
            <SearchSelect
              label="Search by level"
              options={levelOptions}
              dataTestId="filter-level"
              name="level"
              onChange={handleLevelChange}
            />
          </form>
        </section>
        <section className="trips">
          <h2 className="visually-hidden">Trips List</h2>
          <ul className="trip-list">
            {filteredTrips.map((trip, index) => (
              <TripCard
                key={index}
                title={trip.title}
                duration={trip.duration}
                level={trip.level}
                price={trip.price}
                id={trip.id}
                image={trip.image}
              />
            ))}
          </ul>
        </section>
      </main>
      <Footer />
    </>
  );
};
