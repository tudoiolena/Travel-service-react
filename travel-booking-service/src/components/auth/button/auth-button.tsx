import { FC } from "react";

interface IProprs {
  label: string;
}

export const AuthButton: FC<IProprs> = ({ label }) => {
  return (
    <button data-test-id="auth-submit" className="button" type="submit">
      {label}
    </button>
  );
};
