import { useEffect, useState } from "react";
import { Footer, Header, TripDetails } from "../../components";
import { TripModal } from "./components/trip-modal";
import { useParams } from "react-router-dom";
import tripsData from "../../trips.json";

export const Trip = () => {
  interface ITrip {
    title: string;
    duration: number;
    description: string;
    level: string;
    price: number;
    id: string;
    image: string;
  }

  const { id } = useParams();

  const [isModalOpen, setIsModalOpen] = useState(false);
  const [selectedTrip, setSelectedTrip] = useState<ITrip | null>(null);

  const openModal = () => {
    setIsModalOpen(true);
  };

  const closeModal = () => {
    setIsModalOpen(false);
  };

  useEffect(() => {
    const trip = tripsData.find((trip) => trip.id === id);
    setSelectedTrip(trip || null);
  }, [id]);

  if (!selectedTrip) {
    return null;
  }

  return (
    <>
      <Header />
      <main className="trip-page">
        <h1 className="visually-hidden">Travel App</h1>
        <TripDetails
          imageSrc={selectedTrip.image}
          title={selectedTrip.title}
          duration={selectedTrip.duration}
          level={selectedTrip.level}
          description={selectedTrip.description}
          price={selectedTrip.price}
          onBookTrip={openModal}
        />
      </main>
      <Footer />

      <TripModal
        isOpen={isModalOpen}
        onClose={closeModal}
        modalContent={selectedTrip}
      ></TripModal>
    </>
  );
};
