import { FC } from "react";

interface IProps {
  id: string;
  title: string;
  guests: number;
  date: string;
  totalPrice: number;
  onCancel: (id: string) => void;
}

export const BookingItem: FC<IProps> = ({
  id,
  title,
  guests,
  date,
  totalPrice,
  onCancel,
}) => {
  const handleCancel = () => {
    onCancel(id);
  };

  return (
    <li data-test-id="booking" className="booking">
      <h3 data-test-id="booking-title" className="booking__title">
        {title}
      </h3>
      <span data-test-id="booking-guests" className="booking__guests">
        {guests} guests
      </span>
      <span data-test-id="booking-date" className="booking__date">
        {date}
      </span>
      <span data-test-id="booking-total" className="booking__total">
        {totalPrice} $
      </span>
      <button
        data-test-id="booking-cancel"
        className="booking__cancel"
        title="Cancel booking"
        onClick={handleCancel}
      >
        <span className="visually-hidden">Cancel booking</span>×
      </button>
    </li>
  );
};
