export enum Paths {
  MAIN = "/",
  LOGIN = "/sign-in",
  REGISTER = "/sign-up",
  TRIP = "/trip",
  BOOKINGS = "/bookings",
}
