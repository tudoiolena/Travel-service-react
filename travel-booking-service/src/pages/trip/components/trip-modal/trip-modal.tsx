import {
  CSSProperties,
  ChangeEvent,
  FC,
  FormEvent,
  useEffect,
  useState,
} from "react";
import { v4 as uuidv4 } from "uuid";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  modalContent: {
    title: string;
    duration: number;
    price: number;
    level: string;
  };
}

export const TripModal: FC<IProps> = ({ isOpen, onClose, modalContent }) => {
  const [date, setDate] = useState("");
  const [numGuests, setNumGuests] = useState(1);
  const [totalPrice, setTotalPrice] = useState(modalContent.price);

  const handleDateChange = (event: ChangeEvent<HTMLInputElement>) => {
    const newDate = event.target.value;
    setDate(newDate);
  };

  const handleNumGuestsChange = (event: ChangeEvent<HTMLInputElement>) => {
    const newNumGuests = parseInt(event.target.value, 10);
    setNumGuests(newNumGuests);

    const newTotalPrice = modalContent.price * newNumGuests;
    setTotalPrice(newTotalPrice);
  };

  const modalStyle: CSSProperties = {
    display: isOpen ? "flex" : "none",
  };

  const handleSubmit = (event: FormEvent) => {
    event.preventDefault();

    const tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);

    if (new Date(date) < tomorrow) {
      alert("Date should be not earlier than tomorrow");
      return;
    }

    const newBooking = {
      id: uuidv4(),
      title: modalContent.title,
      guests: numGuests,
      date: date,
      totalPrice: totalPrice,
    };

    const existingBookingsJSON = localStorage.getItem("bookings");
    const existingBookings = existingBookingsJSON
      ? JSON.parse(existingBookingsJSON)
      : [];

    const updatedBookings = [...existingBookings, newBooking];
    localStorage.setItem("bookings", JSON.stringify(updatedBookings));

    onClose();
  };

  useEffect(() => {
    if (isOpen) {
      setDate("");
      setNumGuests(1);
      setTotalPrice(modalContent.price);
    }
  }, [isOpen, modalContent.price]);

  return (
    <div>
      <div className="modal" hidden={!isOpen} style={modalStyle}>
        <div data-test-id="book-trip-popup" className="book-trip-popup">
          <button
            data-test-id="book-trip-popup-close"
            className="book-trip-popup__close"
            onClick={onClose}
          >
            ×
          </button>
          <form
            className="book-trip-popup__form"
            autoComplete="off"
            onSubmit={handleSubmit}
          >
            <div className="trip-info">
              <h3
                data-test-id="book-trip-popup-title"
                className="trip-info__title"
              >
                {modalContent.title}
              </h3>
              <div className="trip-info__content">
                <span
                  data-test-id="book-trip-popup-duration"
                  className="trip-info__duration"
                >
                  <strong> {modalContent.duration}</strong> days
                </span>
                <span
                  data-test-id="book-trip-popup-level"
                  className="trip-info__level"
                >
                  {modalContent.level}
                </span>
              </div>
            </div>
            <label className="input">
              <span className="input__heading">Date</span>
              <input
                data-test-id="book-trip-popup-date"
                name="date"
                type="date"
                required
                onChange={handleDateChange}
              />
            </label>
            <label className="input">
              <span className="input__heading">Number of guests</span>
              <input
                data-test-id="book-trip-popup-guests"
                name="guests"
                type="number"
                min="1"
                max="10"
                value={numGuests}
                required
                onChange={handleNumGuestsChange}
              />
            </label>
            <span className="book-trip-popup__total">
              Total:
              <output
                data-test-id="book-trip-popup-total-value"
                className="book-trip-popup__total-value"
              >
                {totalPrice} $
              </output>
            </span>
            <button
              data-test-id="book-trip-popup-submit"
              className="button"
              type="submit"
            >
              Book a trip
            </button>
          </form>
        </div>
      </div>
    </div>
  );
};
